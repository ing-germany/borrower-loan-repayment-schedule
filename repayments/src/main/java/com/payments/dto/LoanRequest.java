package com.payments.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;

public class LoanRequest {

	@ApiModelProperty(notes = "Annuitty loan Amount", required = true, dataType = "Double")
	@NotNull(message = "loan Amount cannot be null.")
	private double loanAmt;

	@ApiModelProperty(notes = "Nominal Interst Rate per year", required = true, dataType = "Double")
	@NotNull(message = "Nominal Interst Rate cannot be null.")
	private double nominalRate;

	@ApiModelProperty(notes = "Duration of loan in months", required = true, dataType = "Integer")
	@Min(1)
	private int duration;

	@ApiModelProperty(notes = "Start Date of disbursement/ payout", required = true, dataType = "Date", example = "01-01-2020")
	@NotNull(message = "Start Date of disbursement/ payout cannot be null and should be in proper format.")
	private Date startDate;

	public double getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(double loanAmt) {
		this.loanAmt = loanAmt;
	}

	public double getNominalRate() {
		return nominalRate;
	}

	public void setNominalRate(double nominalRate) {
		this.nominalRate = nominalRate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}
