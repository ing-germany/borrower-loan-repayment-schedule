package com.payments.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;

public class RePaymentPlan {

	@ApiModelProperty(notes = "Amount that the borrower has to pay back each month.", required = true, dataType = "Double")
	private double borrowerPaymentAmount;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@ApiModelProperty(notes = "Payment date of disbursement/payout.", required = true, dataType = "Double")
	private Date startDate;

	@ApiModelProperty(notes = "Initial OutStanding Principal Amount", dataType = "Double")
	private double initialOutStandingPrincipal;

	@ApiModelProperty(notes = "Interest Amount payable.", required = true, dataType = "Double")
	private double interest;

	@ApiModelProperty(notes = "Principal Amount payable.", required = true, dataType = "Double")
	private double principal;

	@ApiModelProperty(notes = "Remaining Principal Amount payable.", required = true, dataType = "Double")
	private double remainingOutStandingPrincipal;

	public RePaymentPlan(double borrowerPaymentAmount, Date startDate, double initialOutStandingPrincipal,
			double interest, double principal, double remainingOutStandingPrincipal) {
		super();
		this.borrowerPaymentAmount = borrowerPaymentAmount;
		this.startDate = startDate;
		this.initialOutStandingPrincipal = initialOutStandingPrincipal;
		this.interest = interest;
		this.principal = principal;
		this.remainingOutStandingPrincipal = remainingOutStandingPrincipal;
	}

	public RePaymentPlan() {

	}

	public double getBorrowerPaymentAmount() {
		return borrowerPaymentAmount;
	}

	public void setBorrowerPaymentAmount(double borrowerPaymentAmount) {
		this.borrowerPaymentAmount = borrowerPaymentAmount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public double getInitialOutStandingPrincipal() {
		return initialOutStandingPrincipal;
	}

	public void setInitialOutStandingPrincipal(double initialOutStandingPrincipal) {
		this.initialOutStandingPrincipal = initialOutStandingPrincipal;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public double getPrincipal() {
		return principal;
	}

	public void setPrincipal(double principal) {
		this.principal = principal;
	}

	public double getRemainingOutStandingPrincipal() {
		return remainingOutStandingPrincipal;
	}

	public void setRemainingOutStandingPrincipal(double remainingOutStandingPrincipal) {
		this.remainingOutStandingPrincipal = remainingOutStandingPrincipal;
	}

}
