package com.payments.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.payments.dto.LoanRequest;
import com.payments.dto.RePaymentPlan;
import com.payments.exception.ErrorResponse;
import com.payments.exception.ExceptionConstants;
import com.payments.service.RepaymentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(consumes = "application/json", produces = "application/json")
@RestController
public class RepaymentController {

	private static final Logger log = Logger.getLogger(RepaymentController.class.getName());

	@Autowired
	RepaymentService repayService;

	@ApiOperation(value = "Retrieve list of repayment schedule for annuity loan", responseContainer = "List", response = RePaymentPlan.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Ok"), @ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 404, message = "Not found"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	@PostMapping(value = "/generate-plan", produces = "application/json", consumes = "application/json")
	public List<RePaymentPlan> getRePaymentData(@Valid @RequestBody LoanRequest req) {
		log.info("Request for repayament schedule. Inside controller ");
		log.info("validate loan inputs.");
		if (!checkInputs(req))
			throw new ErrorResponse(ExceptionConstants.INVALID_INPUTS, HttpStatus.BAD_REQUEST);
		return repayService.getRepaymentSchedule(req);

	}

	private boolean checkInputs(final LoanRequest req) {
		if (req.getLoanAmt() <= 0 || req.getDuration() <= 0 || req.getNominalRate() <= 0
				|| req.getStartDate().after(new Date()))
			return false;

		return true;
	}
}
