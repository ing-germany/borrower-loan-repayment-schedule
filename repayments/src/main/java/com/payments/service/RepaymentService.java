package com.payments.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payments.calculation.AnnuityCalculation;
import com.payments.calculation.InterestCal;
import com.payments.constant.RepaymentConstants;
import com.payments.constant.RoundOfNumber;
import com.payments.dto.LoanRequest;
import com.payments.dto.RePaymentPlan;

@Service
public class RepaymentService {

	private static final Logger log = Logger.getLogger(RepaymentService.class.getName());

	@Autowired
	AnnuityCalculation annuityCalculation;

	@Autowired
	InterestCal interestCalculation;


	public List<RePaymentPlan> getRepaymentSchedule(LoanRequest req) {
		int loanDuration = req.getDuration();
		double nominalRate = req.getNominalRate();
		List<RePaymentPlan> repaymentPlans = new ArrayList<>();

		double initialOutStandingAmt = req.getLoanAmt();

		double annuity = calculateAnnuity(initialOutStandingAmt, nominalRate, loanDuration);
		annuity = RoundOfNumber.roundToNDecimalPlace(annuity, RepaymentConstants.ROUND_OFF_PLACE);

		for (int i = 1; i <= loanDuration; i++) {
			RePaymentPlan paymentPlan = new RePaymentPlan();

			double interestAmount = calculateInterest(initialOutStandingAmt, nominalRate, annuity, paymentPlan);

			double remainingamount = paymentPlan.getInitialOutStandingPrincipal() - paymentPlan.getPrincipal();
			remainingamount = RoundOfNumber.roundToNDecimalPlace(remainingamount, RepaymentConstants.ROUND_OFF_PLACE);
			
			paymentPlan.setInterest(interestAmount);
			paymentPlan.setBorrowerPaymentAmount(annuity);
			paymentPlan.setRemainingOutStandingPrincipal(remainingamount);
		
			Calendar cal = Calendar.getInstance();
			cal.setTime(req.getStartDate());
			cal.add(Calendar.MONTH, i - 1);

			paymentPlan.setStartDate(cal.getTime());

			// add to repayment schedule list
			repaymentPlans.add(paymentPlan);

			initialOutStandingAmt = remainingamount;
		}
		return repaymentPlans;
	}

	private double calculateInterest(double initialOutStandingAmt, double nominalRate, double annuity,
			RePaymentPlan paymentPlan) {
		double interestAmt = interestCalculation.calculateInterestPerPeriod(initialOutStandingAmt, nominalRate);
		interestAmt = RoundOfNumber.roundToNDecimalPlace(interestAmt, RepaymentConstants.ROUND_OFF_PLACE);

		if (interestAmt > initialOutStandingAmt)
			interestAmt = initialOutStandingAmt;

		double principal = annuity - interestAmt;
		principal = RoundOfNumber.roundToNDecimalPlace(principal, RepaymentConstants.ROUND_OFF_PLACE);

		// During last month principal amount may exceed initial outstanding principal,
		// so this check
		if (principal > initialOutStandingAmt) {
			principal = initialOutStandingAmt;
			annuity = principal + interestAmt;
		}

		paymentPlan.setPrincipal(principal);
		paymentPlan.setInitialOutStandingPrincipal(initialOutStandingAmt);

		return interestAmt;
	}

	private double calculateAnnuity(double initialOutStandingAmt, double nominalRate, int loanDuration) {
		double annuity = annuityCalculation.calculateAnnuity(initialOutStandingAmt,
				nominalRate / RepaymentConstants.NUMBER_OF_MONTHS_IN_YEAR, loanDuration);

		annuity = RoundOfNumber.roundToNDecimalPlace(annuity, RepaymentConstants.ROUND_OFF_PLACE);
		return annuity;
	}

}
